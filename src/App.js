import React from 'react';
import { Switch, Route } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';
import JobsPage from './pages/JobsPage/JobsPage';
import "bootstrap/dist/css/bootstrap.min.css"
import './styles/styles.scss'
import rootReducer from './store';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import Promise from "redux-promise-middleware";
import logger from "redux-logger";
import thunk from "redux-thunk";

const createStoreWithMiddleware = applyMiddleware(Promise, thunk, logger)(createStore);
const store = createStoreWithMiddleware(rootReducer);

function App() {

  return (

    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact component={JobsPage} path="/" />
        </Switch>
      </Router>

    </Provider>
  );
}

export default App;
