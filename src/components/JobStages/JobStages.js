import React from 'react'


export default function JobStage(Props) {

    return (
        <div onClick={() => {
            Props.updateActiveStage(Props.jobID, Props.index)
        }} className="job-container text-center">
            <div className="circles">
                <div className={`out-circle ${Props.isActive ? "active" : ""}`} >
                    <div className={`inside-circle ${Props.isActive ? "active" : ""}`}>

                    </div>
                </div>
            </div>
            <div className={`line ${Props.isActive ? "active" : ""}`}>
                <div>
                    <span className="span">
                        {Props.stage}
                    </span>
                </div>
                <div className="dot">

                </div>
            </div>

        </div>
    )
}