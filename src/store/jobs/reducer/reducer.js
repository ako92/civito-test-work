
export const FETCH_JOBS_SUCCESFULL = "fetch_jobs_succesfull"
export const FETCH_JOBS_ERROR = "fetch_jobs_error"
export const FETCHING_JOBS = "fetching_jobs"

export function JobsReducer(
    state = {
        status: {},
        fetched: false,
        fetching:false,
        error: null
    },
    action) {
    switch (action.type) {
        case FETCH_JOBS_SUCCESFULL: {
            return {
                ...state,
                fetched: true,
                fetching:false,
                status: action.payload
            };
        }
        case FETCHING_JOBS: {
            return {
                ...state,
                fetching:true,
            };
        }
        case FETCH_JOBS_ERROR: {
            return {
                ...state,
                fetched: false,
                fetching:false,
                error: action.payload
            };
        }
        default:
            return state;
    }

}

