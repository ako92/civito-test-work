
import axios from "axios";
import { API_ENDPOINT } from "../../../env";
import { FETCH_JOBS_SUCCESFULL, FETCH_JOBS_ERROR, FETCHING_JOBS } from "../reducer/reducer";


export function FetchJobs() {
  return function (dispatch) {
    dispatch({
      type: FETCHING_JOBS,

    });
    axios
      .get(`${API_ENDPOINT}`)
      .then(response => {
        dispatch({
          type: FETCH_JOBS_SUCCESFULL,
          payload: response.data
        });
      })
      .catch(err => {
        dispatch({
          type: FETCH_JOBS_ERROR,
          payload: err
        });
      });
  };
}
