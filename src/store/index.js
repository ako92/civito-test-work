

import { combineReducers } from "redux";
import { JobsReducer } from "./jobs/reducer/reducer";



const rootReducer = combineReducers({
    jobsReducer: JobsReducer,

});


export default rootReducer;
