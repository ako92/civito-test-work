import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { FetchJobs } from '../../store/jobs/action/action';
import { createSelector } from 'reselect';
import JobStage from '../../components/JobStages/JobStages';


function JobsPage() {

    
    const [jobs, setJobs] = useState()
    const [selectedJobId, setSelectedJobId] = useState()
    const dispatch = useDispatch()
    const jobsSelector = createSelector((state) => state.jobsReducer, state => state.jobsReducer, (jobsReducer) => jobsReducer)
    const jobsReducer = useSelector(jobsSelector)




    useEffect(() => {
        // Get Data from api on page load
        dispatch(FetchJobs())
    }, [])

    useEffect(() => {
        // if job api fetched this will set it to Jobs state to render
        if (jobsReducer.fetched) {
            setJobs(jobsReducer.status)
        }
    }, [jobsReducer])

    function updateActiveStage(id, clickedStageId) {
        // update active prop of job on click on every item of stage
        let temp = jobs.map(item => item.id === id ? { ...item, active: clickedStageId } : item)
        setJobs(temp)
    }

    if (jobs) {
        // if jobs fetched then render elements
        return (

            <div className="jobs-container">
                <div className="select">
                <select defaultValue="0" onChange={(e) => {
                    setSelectedJobId(parseInt(e.target.value))

                }}>
                    <option checked value="0" key="options_0" disabled>انتخاب...</option>
                    {jobs.map(job =>
                        <option key={`options_${job.id}`} value={job.id}>{job.title}</option>
                    )}
                </select>
                </div>
                <div className="job-stages">
                    {selectedJobId && jobs.find(item => item.id === selectedJobId).stages.map((stage, index) =>
                        <JobStage key={stage} updateActiveStage={updateActiveStage} stage={stage}
                             jobID={selectedJobId} index={index + 1}
                            isActive={index+1 <= jobs.find(item => item.id === selectedJobId).active} />

                    )}

                </div>
            </div>

        )
    }
    else if(!jobsReducer.fetching&&jobsReducer.error) {
        // if there's an error
        return (
            <span>خطا</span>
        )
    }
    else {
        // fetching data
        return (
            <span>در حال دریافت اطلاعات مشاغل</span>
        )
    }

}
export default JobsPage;