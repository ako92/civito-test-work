# Civito Test Work

## Introduction

>This web application is a test work for Civito company.  

All you have to do is install dependencies using this command in terminal:

>`npm install`
 
 to make a production build please run:

 >`npm build`
 

 or run it using Hot Dev Server:

>`npm start`

## Technologies and libraries used
React using Create-React-App, Redux, node-sass, react-hooks

